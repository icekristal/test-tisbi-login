'use strict';

const gulp = require('gulp');
const stylus = require('gulp-stylus');
const sass = require('gulp-sass');
const debug = require('gulp-debug');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const gulpIF = require('gulp-if'); //Если да кабы
const newer = require('gulp-newer'); // Сравнение файлов при замене и обновление
const autoprefixer = require('gulp-autoprefixer'); // Добавляет прификсы браузеров
const remember = require('gulp-remember'); // Запоминание файлов в кеше при обновлении
const plumber = require('gulp-plumber'); // Обработчик ошибок
const path = require('path');

const browserSync = require('browser-sync').create();

const del = require('del');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';


gulp.task('styles',function () {
        return gulp.src(['dev/**/*.sass','dev/**/*.scss'],{since:gulp.lastRun('styles')})
            .pipe(plumber())
            .pipe(remember('styles'))
            .pipe(sass())
            .pipe(autoprefixer())

            .pipe(concat('css/style.css'))
        .pipe(gulp.dest('prod'));

});

gulp.task('assets',function () {
    return gulp.src('dev/assets/**',{since: gulp.lastRun('assets')})
        .pipe(debug({title:'assets'}))
        .pipe(gulp.dest('prod'));
});

gulp.task('index',function () {
    return gulp.src('dev/**/*.html',{since : gulp.lastRun('index')})
        .pipe(debug({title:'index'}))
        .pipe(gulp.dest('prod'));
});

gulp.task('clean', function () {
    return del('prod');
});

gulp.task('watch',function () {
    gulp.watch(['dev/**/*.sass','dev/**/*.scss'],gulp.series('styles')).on('unlink',function (filepath) {
        remember.forget('styles',path.resolve(filepath))
    });
    gulp.watch('dev/assets/**/*.*',gulp.series('assets'));

    gulp.watch('dev/**/*.html',gulp.series('index'));
});



gulp.task('build',gulp.series(
    'clean', gulp.parallel('styles','assets','index')
));



gulp.task('server',function () {
    browserSync.init({
        server:'prod'
       // port: 8080
    });
    browserSync.watch('prod/**/*.*').on('change',browserSync.reload)
});

gulp.task('dev',gulp.series('build',gulp.parallel('watch','server')));